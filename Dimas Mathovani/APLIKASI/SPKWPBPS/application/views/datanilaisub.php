<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard App SPK Calon Petugas Pemetaan dan Pemutakhiran </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?= base_url('assets/css/AdminLTE.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('assets/skins/_all-skins.min.css') ?>">
        <link rel="stylesheet" href="<?= base_url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">


    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>S</b>PK</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>SPK Calon Petugas Pemetaan dan Pemutakhiran </b></span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>

                    </a>
                        <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="">
            
                         </a>
            <li>
                      
    <a href="<?php echo base_url('login/logout'); ?>">Logout</a>  
                        </li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
             <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <li>
                            <a href="<?= base_url('dashboard') ?>">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('WeightedProduct') ?>">
                                <i class="fa fa-question"></i> <span>Weighted Product</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a>
                                <i class="fa fa-gear"></i> <span>Kriteria</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu menu-open" style="display: none;">
                                <li><a href="<?= base_url('kriteria') ?>"><i class="fa fa-circle-o"></i> Data Kriteria</a></li>
                                <li><a href="<?= base_url('kriteria/bobot_kriteria') ?>"><i class="fa fa-circle-o"></i> Bobot Kriteria</a></li>
                            </ul>
                        </li>
                        <li class="treeview active">
                            <a>
                                <i class="fa fa-refresh"></i> <span>SubKriteria</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu menu-open" style="display: block;">
                                <li><a href="<?= base_url('subkriteria') ?>"><i class="fa fa-circle-o"></i> Data Subkriteria</a></li>
                                 <li><a href="<?= base_url('Nilaisub') ?>"><i class="fa fa-circle-o"></i>Nilai Subkriteria Wawancara</a></li>
                                <li><a href="<?= base_url('subkriteria/bobot_subkriteria') ?>"><i class="fa fa-circle-o"></i> Bobot Subkriteria</a></li>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a>
                                <i class="fa fa-user"></i> <span>Alternatif</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu menu-open" style="display: none;">
                                <li><a href="<?= base_url('alternatif') ?>"><i class="fa fa-circle-o"></i> Data Alternatif</a></li>
                                <li><a href="<?= base_url('alternatif/evaluasi') ?>"><i class="fa fa-circle-o"></i> Evaluasi Alternatif</a></li>
                                <li><a href="<?= base_url('alternatif/calculate') ?>"><i class="fa fa-circle-o"></i> Hasil WP Alternatif</a></li>
                            </ul>
                        </li>
                    </ul>
                </section>
            </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Nilai 
        <small>Subkriteria</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Nilai</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
      

          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Subkriteria</th>
                  <th>Keterangan</th>
                  <th>Nilai</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>1</td>
                  <td>Kesiapan Pelatihan
                  </td>
                  <td>Siap</td>
                  <td>2</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Kesiapan Pelatihan
                  </td>
                  <td>Ragu-ragu</td>
                  <td>1</td>
                </tr>
               <tr>
                  <td>3</td>
                  <td>Kesiapan Pelatihan
                  </td>
                  <td>Tidak Siap</td>
                  <td>0</td>
                </tr>

                  <tr>
                  <td>4</td>
                  <td>Kesiapan Kegiatan Lapangan
                  </td>
                  <td>Siap</td>
                  <td>2</td>
                </tr>

                  <tr>
                  <td>5</td>
                  <td>Kesiapan Kegiatan Lapangan
                  </td>
                  <td>Ragu-ragu</td>
                  <td>1</td>
                </tr>
                  <tr>
                  <td>6</td>
                  <td>Kesiapan Kegiatan Lapangan
                  </td>
                  <td>Tidak Siap</td>
                  <td>0</td>
                </tr>
                  <tr>
                  <td>7</td>
                  <td>Kesiapan Tugas Lintas Kelurahan
                  </td>
                  <td>Siap</td>
                  <td>2</td>
                </tr>
                  <tr>
                  <td>8</td>
                  <td>Kesiapan Tugas Lintas Kelurahan
                  </td>
                  <td>Ragu-ragu</td>
                  <td>1</td>
                </tr>
                  <tr>
                  <td>9</td>
                  <td>Kesiapan Tugas Lintas Kelurahan
                  </td>
                  <td>Tidak Siap</td>
                  <td>0</td>
                </tr>
                  <tr>
                  <td>10</td>
                  <td>Menguasai Handphone Android
                  </td>
                  <td>Ya</td>
                  <td>1</td>
                </tr>
                  <tr>
                  <td>11</td>
                  <td>Menguasai Handphone Android
                  </td>
                  <td>Tidak</td>
                  <td>0</td>
                </tr>
                  <tr>
                  <td>12</td>
                  <td>Memiliki Handphone Android Sendiri
                  </td>
                  <td>Ada</td>
                  <td>2</td>
                </tr>
                  <tr>
                  <td>13</td>
                  <td>Memiliki Handphone Android Sendiri
                  </td>
                  <td>Sewa/Pinjam</td>
                  <td>1</td>
                </tr>
                  <tr>
                  <td>14</td>
                  <td>Memiliki Handphone Android Sendiri
                  </td>
                  <td>Tidak Ada</td>
                  <td>0</td>
                </tr>
                  <tr>
                  <td>15</td>
                  <td>Spesifikasi Handphone Sesuai
                  </td>
                  <td>Ya</td>
                  <td>1</td>
                </tr>
                  <tr>
                  <td>16</td>
                  <td>Spesifikasi Handphone Sesuai
                  </td>
                  <td>Tidak</td>
                  <td>0</td>
                </tr>
                  <tr>
                  <td>17</td>
                  <td>Dapat Menggunakan Peta Digital
                  </td>
                  <td>Ya</td>
                  <td>1</td>
                </tr>
                  <tr>
                  <td>18</td>
                  <td>Dapat Menggunakan Peta Digital
                  </td>
                  <td>Tidak</td>
                  <td>0</td>
                </tr>
                  <tr>
                  <td>19</td>
                  <td>Dapat Menunjukan Posisi Saat Ini Pada Peta Digital
                  </td>
                  <td>Bisa</td>
                  <td>2</td>
                </tr>
                  <tr>
                  <td>20</td>
                  <td>Dapat Menunjukan Posisi Saat Ini Pada Peta Digital
                  </td>
                  <td>Ragu-ragu</td>
                  <td>1</td>
                </tr>
                  <tr>
                  <td>21</td>
                  <td>Dapat Menunjukan Posisi Saat Ini Pada Peta Digital
                  </td>
                  <td>Tidak Bisa</td>
                  <td>0</td>
                </tr>
                  <tr>
                  <td>22</td>
                  <td>Dapat Praktek Foto SW MAP
                  </td>
                  <td>Sangat Mahir</td>
                  <td>3</td>
                </tr>
                  <tr>
                  <td>23</td>
                  <td>Dapat Praktek Foto SW MAP
                  </td>
                  <td>Mahir</td>
                  <td>2</td>
                </tr>
                  <tr>
                  <td>24</td>
                  <td>Dapat Praktek Foto SW MAP
                  </td>
                  <td>Belum Mahir</td>
                  <td>1</td>
                </tr>
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
        </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>Copyright &copy; 2019 </strong> 
            </footer>
        </div>
        <script src="<?= base_url('assets/js/jquery-2.2.4.js') ?>"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('assets/js/app.min.js') ?>"></script>
        <script src="<?= base_url('bower_components/jquery/dist/jquery.min.js') ?>"></script>
        <script src="<?= base_url('bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
        <script src="<?= base_url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
        <script src="<?= base_url('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
        <script src="<?= base_url('bower_components/fastclick/lib/fastclick.js') ?>"></script>
        <script src="<?= base_url('dist/js/adminlte.min.js') ?>"></script>
        <script src="<?= base_url('dist/js/demo.js') ?>"></script>

    </body>
</html>
